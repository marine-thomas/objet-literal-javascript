const movie = { //Définition de l'objet
    title : 'Forrest Gump', //Cet objet aura une propriété qui prend pour clé title et pour valeur une chaine de caractère
    year : 1994, //Il a aussi une propriété qui a pour clé year et pour valeur un number
    director : 'Robert Zemeckis',
    actors : ['Tom Hanks', 'Robin Wright', 'Gary Sinise'],
    favorite : true,
    note : 4.9,
    duration : 0,
    currentHour : 0,
    currentMin : 0,
    setDuration : function(currentHour, currentMin) { //Cette méthode prend les paramètres nombre d'h et min définis plus haut
        this.currentHour = Math.min(this.currentHour + 1); // On appelle la valeur de la propriété currentHour
        this.currentMin = Math.min(this.currentMin + 1); // On appelle la valeur de la propriété currentMin

        this.duration += (currentHour * 60) + currentMin; // On modifie la valeur de la propriété duration à laquelle on ajoute l'addition des heures(qu'on multiplie par 60 pour obtenir des minutes) et des minutes
        return this.duration; // La fonction retournera la valeur de la propriété duration
    },
}

movie.setDuration(2, 28); // On donne à l'objet movie des valeurs différentes pour les arguments currentHour et currentMin
console.log(movie.duration); // On affiche la durée du film
